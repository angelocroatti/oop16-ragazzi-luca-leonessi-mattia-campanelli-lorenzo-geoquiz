//CHECKSTYLE:OFF
package com.geoquiz.model.test;

import static org.junit.Assert.*;

import java.util.Optional;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.geoquiz.model.quiz.BasicMode;
import com.geoquiz.model.quiz.ExtendedMode;
import com.geoquiz.model.quiz.Quiz;
import com.geoquiz.model.quiz.QuizFactory;

public class QuizTest {
    private static final String ANSWER = "Non ci sono";
    
    @Test
    public void testQuiz() {
        Quiz quiz = null;
        try {
            quiz = QuizFactory.createTypicalDishesQuiz(Optional.of(BasicMode.CHALLENGE));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        assertNotNull(quiz);
        containsQuestion(quiz, "Fish&Chips");
        checkCorrectAnswer(quiz);
        checkIsQuizFinished(quiz);
        try {
            quiz = QuizFactory.createCapitalsQuiz(ExtendedMode.HARD);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        checkAnswerNotInSet(quiz);
        for (int i = 0; i < 3; i++) {
            checkCorrectAnswer(quiz);
        }
        checkIsQuizFinished(quiz);
        final Optional<String> answer = quiz.getCurrentQuestion().getAnswers().stream().findFirst();
        quiz.hitAnswer(answer);
        //if the answer hit is correct current score mustn't be zero
        if (!quiz.isAnswerCorrect()) {
            assertEquals(0, quiz.getCurrentScore());
        } else {
            assertNotEquals(0, quiz.getCurrentScore());
        }
    }
    
    private static void containsQuestion (final Quiz quiz, final String question) {
        while (!quiz.gameOver()) {
            if (quiz.getCurrentQuestion().getQuestion().equals(question)) {
                return;
            }
            quiz.next();
        }
        fail();
    }
    
    private static void checkCorrectAnswer(final Quiz quiz) {
       final String wrongAnswer = quiz.getCurrentQuestion().getAnswers().stream()
                                                                        .filter(a -> !a.equals(quiz.getCorrectAnswer()))
                                                                        .findAny().get();
       quiz.hitAnswer(Optional.of(wrongAnswer));
       assertEquals(quiz.getCurrentScore(), 0);
       assertFalse(quiz.isAnswerCorrect());
    }
    
    private static void checkIsQuizFinished(final Quiz quiz) {
        while (!quiz.gameOver()) {
            quiz.next();
        }
        try {
            quiz.next();
        } catch (IllegalStateException e) {
            assertNotNull(e);
        }
    }
    
    private static void checkAnswerNotInSet(final Quiz quiz) {
        try {
            quiz.hitAnswer(Optional.of(QuizTest.ANSWER));
        } catch (Exception e) {
            assertNotNull(e);
        }
    }
}
