package com.geoquiz.view.label;

/**
 * The menu's labels who a player can see.
 */
public enum Labels {
    /**
     * Username label.
     */
    USERNAME,
    /**
     * Password label.
     */
    PASSWORD;

}
